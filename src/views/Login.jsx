import LoginForm from "../components/Login/LoginForm"

const Login = () => {
    return (
        <>
            {/* Center items vertically */}
            <div className="container flex flex-col mx-auto items-center">
                {/* Center Login page headers (headline and logo) horizontally */}
                <section className="flex items-center">
                    <img
                        src="LostInTranslation_Resources/Logo.png"
                        alt="Lost in Translation logo"
                        className="h-32 my-5 mr-2"
                    />

                    <section className="container flex flex-col">
                        <h1 className="text-3xl border-b-2 border-amber-200">
                            Lost in Translation
                        </h1>
                        <h2 className="text-2xl">Get started</h2>
                    </section>
                </section>

                <LoginForm />
            </div>
        </>
    )
}
export default Login
