import { useSelector } from "react-redux"
import { Navigate } from "react-router-dom"
import Translated from "../components/Translation/Translated"
import TranslationForm from "../components/Translation/TranslationForm"

const Translation = () => {
    const { user } = useSelector((state) => state.userData)

    return (
        <>
            {/* Requires user to be logged in. Redirects to Login if not. */}
            {!user && <Navigate to={"/"} />}
            <div className="container flex flex-col mx-auto items-center">
                <TranslationForm />

                <Translated />
            </div>
        </>
    )
}
export default Translation
