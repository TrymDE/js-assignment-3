import { useSelector } from "react-redux"
import { Navigate } from "react-router-dom"
import UserHistory from "../components/Profile/ProfileHistory"

const Profile = () => {

    const {user} = useSelector((state) => state.userData)
    
    return (
        <>
        {/* Redirect to start if not logged in. Only show history if logged in,
            We need this, since the user object is required for some functions in
            History, and we don't redirect fast enough to not get an error if not logged in. */}
            {!user && <Navigate to="/" />}
            {user && <UserHistory /> }
        </>
    )
}
export default Profile
