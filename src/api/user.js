import { createHeaders } from "."
const apiURL = process.env.REACT_APP_API_URL

/**
 * A function to fetch all users with the provided username from the login api.
 * @param {string} username The username received from the LoginForm input
 * @returns An array containing either an error message or all users with the username from input.
 */
const apiGetUser = async (username) => {
    try {
        const response = await fetch(`${apiURL}?username=${username}`)
        if (!response.ok) {
            console.log("Error when getting user")
            throw new Error("Something went wrong when getting user.")
        }

        const data = await response.json()

        return [null, data]
    } catch (error) {
        return [error.message, []]
    }
}

/**
 * A function to register a new user with the name "username"
 * @param {string} username This is the username we want to register as a new user.
 * @returns An array containing either an error message or the generated user object.
 */
const apiCreateUser = async (username) => {
    try {
        const response = await fetch(apiURL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: [],
            }),
        })
        if (!response.ok) {
            console.log("Error when creating user")
            throw new Error(
                "Something went wrong when creating user " + username
            )
        }

        const data = await response.json()

        return [null, data]
    } catch (error) {
        return [error.message, []]
    }
}

/**
 * Function to log in the user with username. We first try to log in normally.
 * If there are no users with the provided username, create a new user.
 * @param {string} username The username the user wants to use to log in.
 * @returns An array that contains either an error message or a user object. Either an existing user or newly created one.
 */
export const apiLoginUser = async (username) => {
    const [errorOnLogin, user] = await apiGetUser(username)

    if (errorOnLogin) {
        console.log("Error on login: ", errorOnLogin);
        return [errorOnLogin, null]
    }

    // If there exists a user, return it.
    if (user.length > 0) {
        return [null, user.pop()]
    }

    // Otherwise, create a new one
    return await apiCreateUser(username)
}

/**
 * Function to set the translations of a given user.
 * @param {number} userId Id of the user that we want to change translation history for
 * @param {string[]} translations An array of the translations we want to give that user
 * @returns An array containing either an error message or the updated user object, with the changed translations.
 */
export const apiUpdateUser = async (userId, translations) => {
    try {
        const response = await fetch(`${apiURL}/${userId}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: translations,
            }),
        })

        if (!response.ok) {
            throw new Error(
                "Something went wrong when updating translation history."
            )
        }

        const updatedUser = await response.json()

        return [null, updatedUser]
    } catch (error) {
        return [error.message, null]
    }
}
