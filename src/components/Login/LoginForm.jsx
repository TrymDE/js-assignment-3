import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { userFetch } from "../../store/actions/userActions"
import { Navigate } from "react-router-dom"

// Config of username input. Minimum username length is 2, and it is required
const usernameConfig = {
    required: true,
    minLength: 2,
}

/**
 * The LoginForm component. Takes a username from user input and signs in the user.
 * It then redirects to next page
 */
const LoginForm = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm()

    // Used to display correct elements and navigate to next screen when logged in.
    const userData = useSelector((state) => state.userData)
    const { user } = userData
    const dispatch = useDispatch()

    /**
     * Function executed upon submitting form. Registers or logs in user and saves user locally.
     * @param data This is the information received from input. An object containing a username property.
     */
    const onSubmit = (data) => {
        dispatch(userFetch(data.username))
    }

    /**
     * Error handling function.
     * @returns Error message to display if the input is invalid, or the submitForm button if valid.
     */
    const errorMessage = (() => {
        // Shows submit button if username is valid
        if (!errors.username) {
            return (
                <button
                    className="bg-sky-200 rounded p-3 mx-24 my-6 hover:bg-sky-100 active:bg-sky-300"
                    type="submit"
                >
                    Continue
                </button>
            )
        }

        // No username provided
        if (errors.username.type === "required") {
            return <span className="py-2">Username is required</span>
        }

        // Username is too short
        if (errors.username.type === "minLength") {
            return (
                <span className="py-2">
                    Username must be at least two characters long
                </span>
            )
        }
    })()

    return (
        <>
            {/* Redirects user to next page when logged in */}
            {user && <Navigate to="/translation" />}
            <h1 className="text-xl py-2">Enter your name:</h1>

            <form onSubmit={handleSubmit(onSubmit)} className="w-72">
                <fieldset>
                    <label htmlFor="username">Username: </label>

                    <input
                        type="text"
                        placeholder="Your Name"
                        className="border border-black rounded py-1 px-2"
                        {...register("username", usernameConfig)}
                    />
                </fieldset>
                {errorMessage}
                {userData.fetching && <p>Logging in...</p>}
            </form>
        </>
    )
}
export default LoginForm
