import { useSelector } from "react-redux"

const Translated = () => {
    const translation = useSelector((state) => state.translationData)

    return (
        <>
            {/* Only display the translation section if we have something to translate
                I mostly do this because the empty container is very small and not good looking */}
            {translation && (
                <section
                    className="bg-cyan-50 rounded-3xl drop-shadow-xl shadow-md scale-75"
                    id="hand-signs"
                >
                    {/* the active: class is just here for mobile users to be able to see the translation more easily */}
                    <div className="container grid grid-cols-10 grid-rows-4 active:grid-cols-5 auto-cols-max p-5 px-10">
                        {translation.split("").map((character, i) => {
                            const link = `LostInTranslation_Resources/individual_signs/${character}.png`
                            return (
                                <img
                                    src={link}
                                    alt={`Hand sign ${character}`}
                                    key={i}
                                />
                            )
                        })}
                    </div>
                </section>
            )}
        </>
    )
}
export default Translated
