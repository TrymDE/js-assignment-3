import { useEffect } from "react"
import { useForm } from "react-hook-form"
import { useDispatch } from "react-redux"
import { translationSet } from "../../store/actions/translationActions"
import { userAddTranslation } from "../../store/actions/userActions"

const translationConfig = {
    required: true,
    maxLength: 40,
}

const TranslationForm = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm()

    const dispatch = useDispatch()

    // Clear translate form when entering page, so it doesn't show old translations.
    useEffect(() => dispatch(translationSet("")), [dispatch])

    const onSubmit = (data) => {
        // Use regex to keep only english letters, then turn them lowercase.
        // This is what we actually end up translating.
        const translateLetters = data.toTranslate
            .replace(/[^a-zA-Z]/g, "")
            .toLowerCase()

        // Set the translation in redux, so we know what to show in the display
        // Could probably do this without redux, but I felt it was easier this way
        dispatch(translationSet(translateLetters))

        // Add the translation to the users history. We add the full input, special characters and all
        dispatch(userAddTranslation(data.toTranslate))
    }

    // Similar to login. Shows error message if the input is too long or nothing.
    const errorMessage = (() => {
        if (!errors.toTranslate) {
            return null
        }

        if (errors.toTranslate.type === "required") {
            return (
                <p className="font-bold mt-2">
                    Please enter something to translate
                </p>
            )
        }

        if (errors.toTranslate.type === "maxLength") {
            return (
                <p className="font-bold mt-2">
                    Translation is too long. Maximum 40 characters
                </p>
            )
        }
    })()

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)} className="mt-10">
                <fieldset className="flex bg-slate-100 py-1 pr-1 rounded-full h-16 w-max items-center justify-between drop-shadow-md">
                    <input
                        type="text"
                        placeholder="Word(s) to translate"
                        className="text-2xl bg-inherit rounded-full self-stretch px-5"
                        {...register("toTranslate", translationConfig)}
                    />

                    <button
                        type="submit"
                        className="rounded-full font-bold drop-shadow-md bg-blue-800 hover:bg-blue-700 active:bg-blue-900 text-white p-4 place-self-end self-end"
                    >
                        GO
                    </button>
                </fieldset>
            </form>
            {errorMessage}
        </>
    )
}
export default TranslationForm
