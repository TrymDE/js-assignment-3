import { useSelector } from "react-redux"
import { NavLink } from "react-router-dom"

const Navbar = () => {
    const { user } = useSelector((state) => state.userData)

    return (
        <nav className="flex items-center justify-between text-white font-medium bg-violet-600 h-12 min-h-max">
            <ul className="mx-10">
                <li>Lost in Translation</li>
            </ul>

            {/* Only allow user to navigate if logged in. */}
            {user && (
                <section className="grid grid-cols-2 gap-1 mr-2 h-10 place-content-stretch">
                    <NavLink
                        className="hover:bg-violet-500 active:bg-violet-700 rounded px-2"
                        to="/translation"
                    >
                        <p className="my-2">Translation</p>
                    </NavLink>
                    <NavLink
                        className="hover:bg-violet-500 active:bg-violet-700 rounded text-center "
                        to="/profile"
                    >
                        <p className="my-2">Profile</p>
                    </NavLink>
                </section>
            )}
        </nav>
    )
}

export default Navbar
