import { useDispatch, useSelector } from "react-redux"
import { userClearHistory, userLogout } from "../../store/actions/userActions"

const UserHistory = () => {
    const dispatch = useDispatch()
    const { user } = useSelector((state) => state.userData)

    /**
     * Clears the users translation history, both locally and in the api
     */
    const clearHistory = () => {
        if (
            window.confirm(
                "Are you sure you want to clear your history?" +
                    "\nThis action cannot be undone."
            )
        ) {
            dispatch(userClearHistory())
        }
    }

    /**
     * Logs user out. Removes user object from local storage and redux
     */
    const logOut = () => {
        if (window.confirm("Are you sure you want to log out?")) {
            dispatch(userLogout())
        }
    }

    return (
        <>
            <div className="container mx-auto flex flex-col items-center my-8 mb-14">
                <section>
                    <h1 className="text-3xl">{user.username}'s profile</h1>

                    <h1 className="text-2xl">Last 10 translations:</h1>
                    {/* We can just show all saved translations.
                        We limit to 10 when adding translations */}
                    {user && (
                        <ul className="my-5">
                            {user.translations.map((translationText, i) => {
                                return <li key={i}>{translationText}</li>
                            })}
                        </ul>
                    )}
                </section>

                <footer className="fixed bottom-0 pb-5 bg-white">
                    <button
                        className="p-3 mx-2 rounded bg-red-500 font-semibold hover:bg-red-400 active:bg-red-600"
                        onClick={logOut}
                    >
                        Logout
                    </button>

                    <button
                        className="p-3 mx-2 rounded bg-yellow-400 font-semibold hover:bg-yellow-300 active:bg-yellow-500"
                        onClick={clearHistory}
                    >
                        Clear history
                    </button>
                </footer>
            </div>
        </>
    )
}
export default UserHistory
