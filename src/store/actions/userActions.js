// Action types
export const ACTION_USER_SET = "USER:SET-USER"
export const ACTION_USER_FETCH = "USER:FETCH-USER"
export const ACTION_USER_LOGOUT = "USER:LOGOUT"
export const ACTION_USER_ADD_TRANSLATION = "USER:ADD-TRANSLATION"
export const ACTION_USER_CLEAR_HISTORY = "USER:CLEAR-HISTORY"

// Actions
export const userSet = (user) => ({
    type: ACTION_USER_SET,
    payload: user,
})

export const userFetch = (username) => ({
    type: ACTION_USER_FETCH,
    payload: username,
})

export const userLogout = () => ({
    type: ACTION_USER_LOGOUT,
})

export const userAddTranslation = (translation) => ({
    type: ACTION_USER_ADD_TRANSLATION,
    payload: translation,
})

export const userClearHistory = () => ({
    type: ACTION_USER_CLEAR_HISTORY,
})
