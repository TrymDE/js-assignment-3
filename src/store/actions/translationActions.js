// Action types
export const ACTION_TRANSLATION_SET_TRANSLATION = "TRANSLATION:SET-TRANSLATION"

// Actions
export const translationSet = (translation) => ({
    type: ACTION_TRANSLATION_SET_TRANSLATION,
    payload: translation,
})
