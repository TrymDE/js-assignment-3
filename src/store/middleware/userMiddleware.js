import { apiLoginUser, apiUpdateUser } from "../../api/user"
import {
    ACTION_USER_ADD_TRANSLATION,
    ACTION_USER_CLEAR_HISTORY,
    ACTION_USER_FETCH,
    ACTION_USER_LOGOUT,
    userSet,
} from "../actions/userActions"

export const userMiddleware =
    ({ dispatch }) =>
    (next) =>
    (action) => {
        next(action)

        // If we want to fetch user, call the apiLoginUser function
        if (action.type === ACTION_USER_FETCH) {
            apiLoginUser(action.payload).then(([error, user]) => {
                if (error) {
                    console.log("error logging in user:", error)
                }
                // Save the user in local storage.
                localStorage.setItem("translation-user", JSON.stringify(user))
                dispatch(userSet(user))
            })
        }

        // If user logs out, remove from local storage. Removal from redux happens in reducers.
        if (action.type === ACTION_USER_LOGOUT) {
            localStorage.removeItem("translation-user")
        }

        // If we want to add translations, get the user from storage.
        if (action.type === ACTION_USER_ADD_TRANSLATION) {
            const { id, translations } = JSON.parse(
                localStorage.getItem("translation-user")
            )

            // Then, check if we have more than 10 translations. If we do, remove the oldest one.
            if (translations.length >= 10) {
                translations.pop()
            }

            // Finally, update the user in the api, with the translations being the new translation
            // followed by the old translations.
            apiUpdateUser(id, [action.payload, ...translations]).then(
                ([error, updatedUser]) => {
                    if (error) {
                        console.log("Error updating user");
                    }
                    // Save the updated user in local storage.
                    localStorage.setItem(
                        "translation-user",
                        JSON.stringify(updatedUser)
                    )
                    
                    // Update the user in redux
                    dispatch(userSet(updatedUser))

                }
            )

        }

        // If we want to clear the history, use the apiUpdateUser with an empty translations array
        if (action.type === ACTION_USER_CLEAR_HISTORY) {
            const { id } = JSON.parse(
                localStorage.getItem("translation-user")
            )

            apiUpdateUser(id, []).then(
                ([error, updatedUser]) => {
                    if (error) {
                        console.log("Error clearing translations");
                    }
                    localStorage.setItem(
                        "translation-user",
                        JSON.stringify(updatedUser)
                    )
                    
                    dispatch(userSet(updatedUser))

                }
            )
        }
    }
