import { combineReducers } from "redux"
import translationReducer from "./translationReducers"
import userReducer from "./userReducers"

const appReducers = combineReducers({
    userData: userReducer,
    translationData: translationReducer,
})

export default appReducers
