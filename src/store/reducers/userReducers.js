import {
    ACTION_USER_FETCH,
    ACTION_USER_LOGOUT,
    ACTION_USER_SET,
} from "../actions/userActions"

// If there is a user saved locally, log in immediately
const initialState = {
    user: JSON.parse(localStorage.getItem("translation-user")) ?? null,
    error: "",
    fetching: false,
}

// State after logout
const loggedOut = {
    user: null,
    error: "",
    fetching: false,
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_USER_SET:
            // Update the state with a new user
            return { user: action.payload, fetching: false, error: "" }
        case ACTION_USER_FETCH:
            // Remove user, if any. We are now attempting to get the user from api
            return { user: null, fetching: true, error: "" }
        case ACTION_USER_LOGOUT:
            // Set user state to default parameters
            return loggedOut
        default:
            return state
    }
}

export default userReducer
