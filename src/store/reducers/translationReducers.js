import { ACTION_TRANSLATION_SET_TRANSLATION } from "../actions/translationActions";

// Really only used for the translation input to communicate with the display
const translationReducer = (state = "", action) => {
    switch (action.type) {
        case ACTION_TRANSLATION_SET_TRANSLATION:
            return action.payload
        default:
            return state
    }
}

export default translationReducer