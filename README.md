# React Assignment Javascript Noroff
The third assignment for the frontend half of the Java fullstack course at Noroff in the Experis Academy program

In this assignment, we have to create a sign language translator that converts English words and phrases to American sign language, using tools such as React, React Router and the Context API / Redux. Additionally, we have to use the provided Noroff api as a database containing users and their translation history. Both this user API, as well as the solution to the task itself are to be deployed using Heroku.

## Component tree
A pdf of the component tree can be found in the component_tree folder under the name "React assignment.pdf"

## Hosted on Heroku
The application is hosted on heroku, link below:

https://lost-in-translation-tde.herokuapp.com/

## Run locally
The application can also be run locally. To do this, first download the repository to a local folder. Then, run "npm install" inside the project folder. This will install the packages required to deploy the app locally. Finally, once all packages are installed, run "npm start" to start a developer version of the app on the localhost:3000 address.

## Author
Trym Dammann Ellingsen

